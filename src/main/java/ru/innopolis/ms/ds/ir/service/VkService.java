package ru.innopolis.ms.ds.ir.service;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.ms.ds.ir.client.VkClient;
import ru.innopolis.ms.ds.ir.model.Item;
import ru.innopolis.ms.ds.ir.model.Messages;
import ru.innopolis.ms.ds.ir.model.VkResponse;
import ru.innopolis.ms.ds.ir.model.VkUserModel;

import java.util.ArrayList;
import java.util.List;

@Service
public class VkService {

    @Autowired
    private VkClient vkClient;

    @Autowired
    private Gson gson;


    public List<String> getWallPosts(String accesstoken) throws UnirestException {
        HttpResponse<JsonNode> response = vkClient.getWallPosts(accesstoken);
        VkResponse vkResponse = gson.fromJson(response.getBody().toString(), VkResponse.class);

        List<Item> items = vkResponse.getResponse().getItems();
        return getAllStringsFromItems(items);
    }

    public List<String> getWallPostsByString(String string) throws UnirestException {
        VkResponse vkResponse = gson.fromJson(string, VkResponse.class);

        List<Item> items = vkResponse.getResponse().getItems();
        return getAllStringsFromItems(items);
    }


    public List<String> getGroups(String accesstoken) throws UnirestException {
        HttpResponse<JsonNode> response = vkClient.getGroups(accesstoken);
        VkResponse vkResponse = gson.fromJson(response.getBody().toString(), VkResponse.class);
        List<Item> items = vkResponse.getResponse().getItems();
        return getAllStringsFromItems(items);
    }

    public List<String> getGroupsByString(String string) throws UnirestException {
        VkResponse vkResponse = gson.fromJson(string, VkResponse.class);
        List<Item> items = vkResponse.getResponse().getItems();
        return getAllStringsFromItems(items);
    }


    public VkUserModel getUserInfo(String accesstoken) throws UnirestException {
        HttpResponse<JsonNode> response = vkClient.getUserInfo(accesstoken);
        return gson.fromJson(response.getBody().toString(), VkUserModel.class);
    }

    public VkUserModel getUserInfoBySting(String string) throws UnirestException {
        return gson.fromJson(string, VkUserModel.class);
    }


    private List<String> getAllStringsFromItems(List<Item> items) {
        List mes = new ArrayList<String>();
        for (Item item : items) {
            if (item.getText() != null && !item.getText().equals("")) {
                mes.add(item.getText());
            } else if (item.getName() != null && !item.getName().equals("")) {
                mes.add(item.getName());
            } else if (item.getBody() != null && !item.getBody().equals("")) {
                mes.add(item.getBody());
            }
        }
        return mes;

    }


    public List<String> getAllMessages(Messages messages, long id) {
        List mes = new ArrayList<String>();
        for (Item item : messages.getResponse().getItems()) {
            if (!item.getBody().equals("")) {
                mes.add(item.getBody());
            }
        }
        return mes;
    }
}