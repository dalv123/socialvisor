package ru.innopolis.ms.ds.ir.client;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.innopolis.ms.ds.ir.model.Request;
import ru.innopolis.ms.ds.ir.model.Result;

import java.util.*;

@Component
public class SentimentAnalysisClient {

    @Value("${api_key}")
    private String api_key;
    private final String api_url = "https://apiv2.indico.io/sentiment/batch";

    public Map<String, Double> analyzeMessage(List<String> messages) throws UnirestException {
        Request obj = new Request();
        obj.setApiKey(api_key);
        obj.setData(messages);
        Gson gson = new Gson();
        String json = gson.toJson(obj);

        HttpResponse<JsonNode> jsonResponse = Unirest.post(api_url)
                .header("accept", "application/json")
                .body(json)
                .asJson();

        Result result = gson.fromJson(jsonResponse.getBody().toString(), Result.class);
        HashMap<String, Double> analyseMessages = new HashMap<>();
        for (int i = 0; i < messages.size(); i++) {
            analyseMessages.put(messages.get(i), result.getResults().get(i));
        }

        return analyseMessages;
    }





}
