package ru.innopolis.ms.ds.ir.service;

import com.google.gson.Gson;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.ms.ds.ir.client.SentimentAnalysisClient;
import ru.innopolis.ms.ds.ir.model.AnalysisResult;

import java.util.*;

import static ru.innopolis.ms.ds.ir.util.Utils.round;

@Component
public class SentimentAnalysisService {

    @Autowired
    private SentimentAnalysisClient client;

    @Autowired
    private Gson gson;

    public Map<String, Double> sentimentAnalysis(List<String> messages) throws UnirestException {
        return client.analyzeMessage(messages);
    }

    public String getResponse(Map<String, Double> stringDoubleMap) {
        int neg = 0;
        int net = 0;
        int pos = 0;
        int totalSize = stringDoubleMap.size();

        for (Double aDouble : stringDoubleMap.values()) {
            double value = aDouble.doubleValue();
            if (value < 0.3333) {
                neg++;
            } else if (value > 0.6666) {
                pos++;
            } else {
                net++;
            }
        }

        AnalysisResult result = new AnalysisResult();
        result.setMessagesValues(stringDoubleMap);
        result.setMessageCount(totalSize);
        result.setNegativePercent(round((double) neg / totalSize, 2));
        result.setNeutralPercent(round((double) net / totalSize, 2));
        result.setPositivePercent(round((double) pos / totalSize, 2));
        String json = gson.toJson(result);
        return json;
    }

    public String[] suspecionGroupAnalysis(Map<String, Double> groups) {
        groups = sortByValue(groups);
        StringBuilder stringBuilder = new StringBuilder("");
        int i = 0;
        for (String key : groups.keySet()) {
            if (i > 4 || groups.get(key) > 0.33) {
                break;
            }
            stringBuilder.append("<li>" + key + "</li>");
            i++;
        }
        String[] reponse = new String[2];
        reponse[0] = stringBuilder.toString();
        reponse[1] = i + "";
        return reponse;
    }


    public String[] suspecionWallAnalysis(Map<String, Double> wallPosts) {
        wallPosts = sortByValue(wallPosts);
        StringBuilder stringBuilder = new StringBuilder("");
        int i = 0;
        for (String key : wallPosts.keySet()) {
            if (i > 5 || wallPosts.get(key) > 0.33) {
                break;
            } else if (key.contains("http://") && key.length() < 80) {
                continue;
            } else if (key.length() < 34) {
                stringBuilder.append("<li>" + key + "</li>");
            } else {
                stringBuilder.append("<li>" + key.substring(0, 32) + "...</li>");
            }
            i++;
        }

        String[] resp = new String[2];
        resp[0] = stringBuilder.toString();
        resp[1] = i+"";

        return resp;
    }


    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

}
