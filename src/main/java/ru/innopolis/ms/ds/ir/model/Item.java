
package ru.innopolis.ms.ds.ir.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "date",
    "out",
    "user_id",
    "read_state",
    "title",
    "body",
    "random_id"
})
public class Item {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("owner_id")
    private Integer owner_id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("date")
    private Integer date;
    @JsonProperty("out")
    private Integer out;
    @JsonProperty("user_id")
    private Integer userId;
    @JsonProperty("read_state")
    private Integer readState;
    @JsonProperty("title")
    private String title;
    @JsonProperty("body")
    private String body;
    @JsonProperty("text")
    private String text;
    @JsonProperty("random_id")
    private Integer randomId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Integer getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(Integer owner_id) {
        this.owner_id = owner_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("date")
    public Integer getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(Integer date) {
        this.date = date;
    }

    @JsonProperty("out")
    public Integer getOut() {
        return out;
    }

    @JsonProperty("out")
    public void setOut(Integer out) {
        this.out = out;
    }

    @JsonProperty("user_id")
    public Integer getUserId() {
        return userId;
    }

    @JsonProperty("user_id")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @JsonProperty("read_state")
    public Integer getReadState() {
        return readState;
    }

    @JsonProperty("read_state")
    public void setReadState(Integer readState) {
        this.readState = readState;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("body")
    public String getBody() {
        return body;
    }

    @JsonProperty("body")
    public void setBody(String body) {
        this.body = body;
    }

    @JsonProperty("random_id")
    public Integer getRandomId() {
        return randomId;
    }

    @JsonProperty("random_id")
    public void setRandomId(Integer randomId) {
        this.randomId = randomId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
