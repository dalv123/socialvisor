package ru.innopolis.ms.ds.ir.controller;

import com.mashape.unirest.http.exceptions.UnirestException;
import io.indico.api.utils.IndicoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.ms.ds.ir.model.Messages;
import ru.innopolis.ms.ds.ir.model.VkUserModel;
import ru.innopolis.ms.ds.ir.service.SentimentAnalysisService;
import ru.innopolis.ms.ds.ir.service.VkService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static ru.innopolis.ms.ds.ir.client.VkClient.BASE_URL;
import static ru.innopolis.ms.ds.ir.client.VkClient.MESSAGE_GET;
import static ru.innopolis.ms.ds.ir.util.Utils.split;

@Controller
public class MainController {


    @Autowired
    private SentimentAnalysisService sentimentAnalysis;

    @Autowired
    private VkService vkService;

    /**
     * Сюда приходят сообщения
     *
     * @param messages
     * @return
     */
    @RequestMapping(value = "/analysis", method = RequestMethod.POST)
    @ResponseBody
    public String incomeMessage(@RequestBody Messages messages, @RequestParam long userId) throws IOException, IndicoException {

        List<String> allMessages = vkService.getAllMessages(messages, userId);
        Map<String, Double> stringDoubleMap = null;
        try {
            stringDoubleMap = sentimentAnalysis.sentimentAnalysis(allMessages);
        } catch (UnirestException e) {
            //Show error
            e.printStackTrace();
        }

        return sentimentAnalysis.getResponse(stringDoubleMap);
    }


    @RequestMapping(value = "/messages", method = RequestMethod.POST)
    public String messagesPost(@RequestParam String link,@RequestParam String user, @RequestParam String groups,@RequestParam String walls, Model model) {
        Map<String, String> split = null;
        try {
             split = split(link.replace('#', '?'));
        }catch (Exception e){

        }

        String accesToken = split.get("access_token");
        String userId = split.get("user_id");
        String vkLink = BASE_URL + MESSAGE_GET + accesToken + "&callback=callbackFunc";

        model.addAttribute("vk_link", vkLink);
        model.addAttribute("user_id", Long.valueOf(userId));


        //User information
        VkUserModel vkUserModel = null;
        try {
            vkUserModel = vkService.getUserInfoBySting(user);
        } catch (UnirestException e) {
            //Show user error
            e.printStackTrace();
        }

        List<String> groups2 = null;
        try {
            groups2 = vkService.getGroupsByString(groups);
        } catch (UnirestException e) {
            //Show user error
            e.printStackTrace();
        }

        List<String> wallPosts = null;
        try {
            wallPosts = vkService.getWallPostsByString(walls);
        } catch (UnirestException e) {
            //Show user error
            e.printStackTrace();
        }


        String photoMax = vkUserModel.getResponse().get(0).getPhoto_max();
        String userName = vkUserModel.getResponse().get(0).getFirst_name() + " " + vkUserModel.getResponse().get(0).getLast_name();
        String education = vkUserModel.getResponse().get(0).getEducation();
        Integer followersCount = vkUserModel.getResponse().get(0).getFollowers_count();


        model.addAttribute("vk_photo", photoMax);
        model.addAttribute("vk_name", userName);
        model.addAttribute("education", education);
        model.addAttribute("followers_count", followersCount);
        model.addAttribute("groups_count", groups2.size());
        model.addAttribute("walls_count", wallPosts.size());

        //Group analysis
        Map<String, Double> groupsRating = null;
        try {
            groupsRating = sentimentAnalysis.sentimentAnalysis(groups2);
        } catch (UnirestException e) {
            //Show user error
            e.printStackTrace();
        }

        String res[] = sentimentAnalysis.suspecionGroupAnalysis(groupsRating);
        String sus_groups = res[0];

        model.addAttribute("sus_groups", sus_groups);
        model.addAttribute("sus_groups_count", res[1]);

        //Wall analysis
        String sus_walls;
        Map<String, Double> wallRating = null;
        try {
            wallRating = sentimentAnalysis.sentimentAnalysis(wallPosts);
        } catch (UnirestException e) {
            //Show user error
            e.printStackTrace();
        }

        res = sentimentAnalysis.suspecionWallAnalysis(wallRating);
        sus_walls = res[0];
        model.addAttribute("sus_walls", sus_walls);
        model.addAttribute("sus_walls_count", res[1]);

        return "index";
    }

    /*
    "response":{
      "count":97225,
      "items":[
         {
            "id":158249,
            "date":1510918184,
            "out":0,
            "user_id":22776169,
            "read_state":1,
            "title":"",
            "body":"Тест",
            "random_id":315044255
         }
      ]
   }
     */



    @RequestMapping(value = "/.well-known/acme-challenge/cc74vH6J82MK7i3t_QZOOpvjTaT1XuPSOf5Whxh9Jq4")
    @ResponseBody
    public String ssl() {
        return "cc74vH6J82MK7i3t_QZOOpvjTaT1XuPSOf5Whxh9Jq4.TVEcpoJ7uz-pxEPJYF2mH1TA9C1VGD4b8RtFsQWTKLU";
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String signup() {
        return "sign-up-vk";
    }

}
