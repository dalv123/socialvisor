package ru.innopolis.ms.ds.ir.model;

import java.util.HashMap;
import java.util.Map;

public class AnalysisResult {

    private Map<String,Double> messagesValues;

    private double negativePercent;
    private double neutralPercent;
    private double positivePercent;

    private int messageCount;
    private int postCount;
    private int groupCount;

    private HashMap<String,Double> groupsValues;
    private HashMap<String,Double> postsValues;

    public AnalysisResult() {
    }

    public AnalysisResult(Map<String, Double> messagesValues, double negativePercent, double neutralPercent, double positivePercent, int messageCount, int postCount, int groupCount, HashMap<String, Double> groupsValues, HashMap<String, Double> postsValues) {
        this.messagesValues = messagesValues;
        this.negativePercent = negativePercent;
        this.neutralPercent = neutralPercent;
        this.positivePercent = positivePercent;
        this.messageCount = messageCount;
        this.postCount = postCount;
        this.groupCount = groupCount;
        this.groupsValues = groupsValues;
        this.postsValues = postsValues;
    }

    public Map<String, Double> getMessagesValues() {
        return messagesValues;
    }

    public void setMessagesValues(Map<String, Double> messagesValues) {
        this.messagesValues = messagesValues;
    }

    public double getNegativePercent() {
        return negativePercent;
    }

    public void setNegativePercent(double negativePercent) {
        this.negativePercent = negativePercent;
    }

    public double getNeutralPercent() {
        return neutralPercent;
    }

    public void setNeutralPercent(double neutralPercent) {
        this.neutralPercent = neutralPercent;
    }

    public double getPositivePercent() {
        return positivePercent;
    }

    public void setPositivePercent(double positivePercent) {
        this.positivePercent = positivePercent;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public int getPostCount() {
        return postCount;
    }

    public void setPostCount(int postCount) {
        this.postCount = postCount;
    }

    public int getGroupCount() {
        return groupCount;
    }

    public void setGroupCount(int groupCount) {
        this.groupCount = groupCount;
    }

    public HashMap<String, Double> getGroupsValues() {
        return groupsValues;
    }

    public void setGroupsValues(HashMap<String, Double> groupsValues) {
        this.groupsValues = groupsValues;
    }

    public HashMap<String, Double> getPostsValues() {
        return postsValues;
    }

    public void setPostsValues(HashMap<String, Double> postsValues) {
        this.postsValues = postsValues;
    }
}
