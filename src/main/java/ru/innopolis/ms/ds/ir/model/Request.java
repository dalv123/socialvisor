
package ru.innopolis.ms.ds.ir.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "api_key",
    "data"
})
public class Request {

    @JsonProperty("api_key")
    private String api_key;
    @JsonProperty("data")
    private List<String> data = null;

    @JsonProperty("api_key")
    public String getApiKey() {
        return api_key;
    }

    @JsonProperty("api_key")
    public void setApiKey(String apiKey) {
        this.api_key = apiKey;
    }

    @JsonProperty("data")
    public List<String> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<String> data) {
        this.data = data;
    }

}
