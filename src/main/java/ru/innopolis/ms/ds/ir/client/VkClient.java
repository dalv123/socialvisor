package ru.innopolis.ms.ds.ir.client;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.stereotype.Component;

@Component
public class VkClient {

    public static final String BASE_URL = "https://api.vk.com/method/";

    public static final String MESSAGE_GET = "messages.search?q=*&preview_length=0&count=100&v=5.52&access_token=";
    public static final String USER_GET = "users.get?fields=photo_max,education,last_seen,followers_count&v=5.52&access_token=";
    public static final String GROUPS_GET = "groups.get?extended=1&count=200&v=5.52&access_token=";
    public static final String WALL_GET = "wall.get?extended=0&count=100&v=5.52&access_token=";


    public HttpResponse<JsonNode> getHttpResponse(String link, String accessToken) throws UnirestException {
        HttpResponse<JsonNode> wallsResponse;
        wallsResponse = Unirest.get(link + accessToken)
                .header("accept", "application/json")
                .asJson();
        return wallsResponse;
    }


    public HttpResponse<JsonNode> getWallPosts(String accessToken) throws UnirestException {
        return getHttpResponse(BASE_URL + WALL_GET, accessToken);
    }

    public HttpResponse<JsonNode> getGroups(String accessToken) throws UnirestException {
        return getHttpResponse(BASE_URL + GROUPS_GET, accessToken);
    }


    public HttpResponse<JsonNode> getUserInfo(String accessToken) throws UnirestException {
        return getHttpResponse(BASE_URL + USER_GET, accessToken);
    }




}
